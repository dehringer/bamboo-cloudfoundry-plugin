/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.utils;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.*;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.COMMAND;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.ENVIRONMENT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.INSTANCES;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.MEMORY;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.SERVICES;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.ROUTES;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang.StringUtils;
import org.gaptap.bamboo.cloudfoundry.client.ApplicationConfiguration;
import org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.task.CommonTaskContext;

/**
 * @author David Ehringer
 * 
 */
public class ApplicationConfigurationMapper {

    /**
     * Assumes that data has already been validated by
     * {@link PushTaskConfigurator#validate(com.atlassian.bamboo.collections.ActionParametersMap, com.atlassian.bamboo.utils.error.ErrorCollection)}
     * 
     * @param taskContext
     * @return
     */
    public ApplicationConfiguration from(CommonTaskContext taskContext) {
        ConfigurationMap configurationMap = taskContext.getConfigurationMap();
        ApplicationConfiguration.Builder builder = ApplicationConfiguration.builder()
                .name(configurationMap.get(APPLICATION_NAME))
                .memory(Integer.parseInt(configurationMap.get(MEMORY)))
                .instances(Integer.parseInt(configurationMap.get(INSTANCES)))
                .environment(parseEnvironmentList(configurationMap.get(ENVIRONMENT)))
                .addAllServiceBindings(parseCommaSeparateList(configurationMap.get(SERVICES)));
        if (StringUtils.isEmpty(configurationMap.get(BUILDPACK_URL))) {
            builder.buildpacks(Collections.EMPTY_LIST);
        } else {
            List<String> buildpacks = new ArrayList<>();
            buildpacks.add(configurationMap.get(BUILDPACK_URL));
            builder.buildpacks(buildpacks);
        }
        if (StringUtils.isEmpty(configurationMap.get(COMMAND))) {
            builder.command("");
        } else {
            builder.command(configurationMap.get(COMMAND));
        }
        if (StringUtils.isEmpty(configurationMap.get(DISK_QUOTA))) {
            builder.diskQuota(ApplicationConfiguration.DEFAULT_DISK_QUOTA);
        } else {
            builder.diskQuota(Integer.parseInt(configurationMap.get(DISK_QUOTA)));
        }
        if(configurationMap.getAsBoolean(NO_HOSTNAME)){
            builder.noHostname(true);
            for(String domain: parseCommaSeparateList(configurationMap.get(ROUTES))){
                builder.domain(domain);
            }
        } else {
            builder.addAllRoutes(parseCommaSeparateList(configurationMap.get(ROUTES)));
        }
        if (StringUtils.isEmpty(configurationMap.get(HEALTH_CHECK_TIMEOUT))) {
            builder.healthCheckTimeout(ApplicationConfiguration.PLATFORM_DEFAULT_TIMEOUT);
        } else {
            builder.healthCheckTimeout(Integer.parseInt(configurationMap.get(HEALTH_CHECK_TIMEOUT)));
        }
        return builder.build();
    }

    public static List<String> parseCommaSeparateList(String list) {
        List<String> result = new ArrayList<String>();
        if(list != null) {
            list = list.trim();
            for (String item : list.split(",")) {
                if (item.trim().length() > 0) {
                    result.add(item.trim());
                }
            }
        }
        return result;
    }

    public static Map<String, String> parseEnvironmentList(String list) {
        Map<String, String> env = new HashMap<String, String>();
        for (String line : list.split("\n")) {
            String[] var = line.trim().split("=");
            if (var.length == 2) {
                env.put(var[0].trim(), var[1].trim());
            } else if (var.length == 1) {
                if (!"".equals(var[0].trim())) {
                    env.put(var[0].trim(), "");
                }
            } else if (var.length == 0) {
                // empty, do nothing
            } else {
                // I think we need an enhancement to handle all scenarios here.
                int index = line.indexOf('=');
                env.put(line.substring(0, index).trim(), line.substring(index + 1).trim());
            }
        }
        return env;
    }
}
