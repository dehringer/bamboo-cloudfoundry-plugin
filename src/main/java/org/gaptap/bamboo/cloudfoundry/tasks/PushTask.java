/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.variable.VariableDefinitionContext;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.gaptap.bamboo.cloudfoundry.cli.CliFormatter;
import org.gaptap.bamboo.cloudfoundry.client.ApplicationConfiguration;
import org.gaptap.bamboo.cloudfoundry.client.BlueGreenConfiguration;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;
import org.gaptap.bamboo.cloudfoundry.client.PushConfiguration;
import org.gaptap.bamboo.cloudfoundry.tasks.utils.ApplicationConfigurationMapper;
import org.gaptap.bamboo.cloudfoundry.tasks.utils.YamlToApplicationConfigurationMapper;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.ADVANCED_OPTIONS_ENABLED;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.ADVANCED_STALE_ENV_DISABLE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.ADVANCED_STALE_ROUTES_DISABLE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_CONFIG_OPTION_MANUAL;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_CONFIG_OPTION_YAML;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_LOCATION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_LOCATION_OPTION_FILE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.BLUE_GREEN_CUSTOM_DARK_CONFIG;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.BLUE_GREEN_ENABLED;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.BLUE_HEALTH_CHECK_ENDPOINT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.BLUE_HEALTH_CHECK_SKIP_SSL_VALIDATION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.DARK_APP_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.DARK_APP_ROUTE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.DIRECTORY;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.FILE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.STAGING_TIMEOUT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.STARTUP_TIMEOUT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.SELECTED_APP_CONFIG_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.START;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.YAML_FILE;

/**
 * A {@link TaskType} to push an application, syncing changes if it exists.
 * 
 * @author David Ehringer
 */
public class PushTask extends AbstractCloudFoundryTask {

    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(PushTask.class);

    private final ApplicationConfigurationMapper configMapper = new ApplicationConfigurationMapper();

    public PushTask(EncryptionService encryptionService) {
        super(encryptionService);
    }

    @Override
    @NotNull
    public TaskResult doExecute(@NotNull CommonTaskContext taskContext)
            throws TaskException {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        ConfigurationMap configMap = taskContext.getConfigurationMap();

        CloudFoundryService cloudFoundry = getCloudFoundryService(taskContext);
        CliFormatter cliFormatter = new CliFormatter(buildLogger);

        ApplicationConfiguration appConfig = buildApplicationConfig(taskContext, buildLogger, configMap, cloudFoundry);
        PushConfiguration pushConfiguration = buildPushConfig(taskContext, configMap);
        BlueGreenConfiguration blueGreenConfig = buildBlueGreenConfig(buildLogger, configMap);

        logConfiguration(buildLogger, appConfig, blueGreenConfig);

        TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(
                taskContext).success();

        buildLogger.addBuildLogEntry("Pushing app " + appConfig.name() + " " + getLoginContext(taskContext));
        try {
            if(blueGreenConfig.isEnabled()){
                CountDownLatch latch = new CountDownLatch(1);
                cloudFoundry
                        .push(appConfig, pushConfiguration, blueGreenConfig)
                        .timeout(Duration.ofMinutes(20))
                        .subscribe(applicationDetail -> {
                                    cliFormatter.cfApp(applicationDetail);
                                },
                                throwable -> {
                                    LOG.error("Unable to push app with a blue/green deployment", throwable);
                                    buildLogger.addErrorLogEntry("Unable to push app with a blue/green deployment: " + throwable.getMessage());
                                    taskResultBuilder.failedWithError();
                                    latch.countDown();
                                },
                                latch::countDown);

                latch.await();
            } else {
                CountDownLatch latch = new CountDownLatch(1);
                cloudFoundry
                        .push(appConfig, pushConfiguration)
                        .timeout(Duration.ofMinutes(20))
                        .subscribe(applicationDetail -> {
                                    cliFormatter.cfApp(applicationDetail);
                                },
                                throwable -> {
                                    LOG.error("Unable to push app", throwable);
                                    buildLogger.addErrorLogEntry("Unable to push app: " + throwable.getMessage());
                                    taskResultBuilder.failedWithError();
                                    latch.countDown();
                                },
                                latch::countDown);

                latch.await();
            }
        } catch (InterruptedException e) {
            buildLogger.addErrorLogEntry("Unable to complete task due to unknown error: " + e.getMessage());
            taskResultBuilder.failedWithError();
        }

        return taskResultBuilder.build();
    }

    private void logConfiguration(BuildLogger buildLogger, ApplicationConfiguration appConfig, BlueGreenConfiguration blueGreenConfig) {
        BuildLoggerFacade buildLoggerFacade = new BuildLoggerFacade(buildLogger);
        appConfig.logConfiguration(buildLoggerFacade);
        if(blueGreenConfig.isEnabled()){
            blueGreenConfig.logConfiguration(buildLoggerFacade);
        }
    }

    @NotNull
    private ApplicationConfiguration buildApplicationConfig(@NotNull CommonTaskContext taskContext, BuildLogger buildLogger,
                                                            ConfigurationMap configMap, CloudFoundryService cloudFoundry) throws TaskException {
        String configOption = configMap.get(SELECTED_APP_CONFIG_OPTION);
        ApplicationConfiguration appConfig = null;
        if (APP_CONFIG_OPTION_MANUAL.equals(configOption)) {
            appConfig = configMapper.from(taskContext);
        } else if (APP_CONFIG_OPTION_YAML.equals(configOption)) {
            appConfig = extractAppConfigFromManifest(taskContext, configMap, appConfig, cloudFoundry);
        } else {
            handleInvalidAppConfig(buildLogger, configOption);
        }
        return appConfig;
    }

    private ApplicationConfiguration extractAppConfigFromManifest(
            CommonTaskContext taskContext, ConfigurationMap configMap,
            ApplicationConfiguration appConfig, CloudFoundryService cloudFoundry) throws TaskException {
        String defaultDomain = getDefaultDomain(taskContext, cloudFoundry);
        String manifest = configMap.get(YAML_FILE);

        File rootDir = taskContext.getRootDirectory();
        File manifestFile = new File(rootDir, manifest);
        taskContext.getBuildLogger().addBuildLogEntry(
                "Loading application configuration from "
                        + manifestFile.getAbsolutePath());
        InputStream input = null;
        try {
            input = new FileInputStream(manifestFile);
        } catch (FileNotFoundException e) {
            File parentDir =  new File(rootDir, manifestFile.getParent());
            FileFinder finder = new FileFinder(parentDir);
            String extension  = Iterables.getLast(Lists.newArrayList(manifest.split("\\.")), "yaml");
            List<File> matching = finder.findFilesMatching(String.format("*.%s", extension));
            String message = "Manifest file not found: "
                    + manifestFile.getAbsolutePath();
            if(!matching.isEmpty()) {
                StringBuilder otherFiles = new StringBuilder();
                matching.forEach(file -> otherFiles.append("\n").append(file.getName()));

                message = message + ". These files were found in the same directory: " + otherFiles.toString();
            }

            taskContext.getBuildLogger().addErrorLogEntry(message, e);
            throw new TaskException(message, e);
        }

        BuildLoggerFacade logger = new BuildLoggerFacade(taskContext.getBuildLogger());
        YamlToApplicationConfigurationMapper yamlMapper = new YamlToApplicationConfigurationMapper(
                defaultDomain, logger);
        return yamlMapper.from(input, getBambooVariables(taskContext));
    }

    private String getDefaultDomain(CommonTaskContext taskContext, CloudFoundryService cloudFoundry) {
        return cloudFoundry
                .defaultDomain(getOrganization(taskContext))
                .block();
    }

    private Map<String, String> getBambooVariables(CommonTaskContext taskContext) {
        Map<String, String> variables = Maps.newHashMap();
        for (VariableDefinitionContext context : taskContext.getCommonContext()
                .getVariableContext().getEffectiveVariables().values()) {
            variables.put(context.getKey(), context.getValue());
        }
        return variables;
    }

    private void handleInvalidAppConfig(BuildLogger buildLogger,
            String configOption) throws TaskException {
        String message = "Task is configured with an invalid application configuration option: "
                + configOption
                + ". The task configuration is in an invalid state.";
        buildLogger.addErrorLogEntry(message);
        throw new TaskException(message);
    }

    private PushConfiguration buildPushConfig(CommonTaskContext taskContext, ConfigurationMap configMap) throws TaskException {
        PushConfiguration.Builder builder = PushConfiguration.builder()
                .start(configMap.getAsBoolean(START))
                .applicationFile(getApplicationFile(taskContext));
        if(!StringUtils.isEmpty(configMap.get(STARTUP_TIMEOUT))){
            builder.startupTimeout(Integer.parseInt(configMap.get(STARTUP_TIMEOUT)));
        }
        if(!StringUtils.isEmpty(configMap.get(STAGING_TIMEOUT))){
            builder.stagingTimeout(Integer.parseInt(configMap.get(STAGING_TIMEOUT)));
        }
        if(configMap.getAsBoolean(ADVANCED_OPTIONS_ENABLED)){
            builder.disableUnmappingStaleRoutes(configMap.getAsBoolean(ADVANCED_STALE_ROUTES_DISABLE));
            builder.disableUnsettingStaleEnvVars(configMap.getAsBoolean(ADVANCED_STALE_ENV_DISABLE));
        }
        return builder.build();
    }

    private BlueGreenConfiguration buildBlueGreenConfig(BuildLogger buildLogger, ConfigurationMap configMap) {
        BlueGreenConfiguration.Builder builder = BlueGreenConfiguration.builder()
                .enabled(configMap.getAsBoolean(BLUE_GREEN_ENABLED))
                .useCustomDarkAppConfiguration(configMap.getAsBoolean(BLUE_GREEN_CUSTOM_DARK_CONFIG))
                .healthCheckEndpoint(configMap.get(BLUE_HEALTH_CHECK_ENDPOINT))
                .skipSslValidation(configMap.getAsBoolean(BLUE_HEALTH_CHECK_SKIP_SSL_VALIDATION));
        if(configMap.getAsBoolean(BLUE_GREEN_CUSTOM_DARK_CONFIG)){
            builder
                    .customDarkAppName(configMap.get(DARK_APP_NAME))
                    .customDarkRoute(configMap.get(DARK_APP_ROUTE));
        }
        return builder.build();
    }

    private File getApplicationFile(CommonTaskContext taskContext)
            throws TaskException {
        File baseDir = taskContext.getRootDirectory();
        String location = taskContext.getConfigurationMap().get(APP_LOCATION);
        if (APP_LOCATION_OPTION_FILE.equals(location)) {
            String fileName = taskContext.getConfigurationMap().get(FILE);
            FileFinder finder = new FileFinder(baseDir);
            List<File> matching = finder.findFilesMatching(fileName);
            if (matching.size() != 1) {
                throw new TaskException("Found " + matching.size()
                        + " files matching the pattern " + fileName
                        + ". Only one matching file is expected.");
            }
            return matching.get(0);
        } else {
            String directory = taskContext.getConfigurationMap().get(DIRECTORY);
            return new File(baseDir, directory);
        }
    }
}