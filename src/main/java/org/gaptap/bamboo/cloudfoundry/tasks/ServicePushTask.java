/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;
import org.gaptap.bamboo.cloudfoundry.client.ServiceConfiguration;
import org.gaptap.bamboo.cloudfoundry.tasks.utils.YamlToServiceConfigurationMapper;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServicePushTaskConfigurator.FILE;

/**
 * A {@link TaskType} to create or update a service.
 * 
 * @author David Ehringer
 */
public class ServicePushTask extends AbstractCloudFoundryTask {

    private final YamlToServiceConfigurationMapper serviceMapper = new YamlToServiceConfigurationMapper();

    public ServicePushTask(EncryptionService encryptionService) {
        super(encryptionService);
    }

    @Override
    @NotNull
    public TaskResult doExecute(@NotNull CommonTaskContext taskContext) throws TaskException {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext).success();

        InputStream input = getManifestFile(taskContext);
        CloudFoundryService cloudFoundry = getCloudFoundryService(taskContext);
        try{
            List<ServiceConfiguration> serviceConfigurations = serviceMapper.from(input);
            for (ServiceConfiguration serviceConfiguration: serviceConfigurations){
                buildLogger.addBuildLogEntry("Pushing service " + serviceConfiguration.getServiceInstance() + " " + getLoginContext(taskContext));
                doSubscribe(cloudFoundry.pushService(serviceConfiguration), "Unable to push service.", buildLogger, taskResultBuilder);
            }
        } catch (Exception e){
            taskResultBuilder.failedWithError();
        }

        return taskResultBuilder.build();
    }

    @NotNull
    private InputStream getManifestFile(@NotNull CommonTaskContext taskContext) throws TaskException {
        String manifestName = taskContext.getConfigurationMap().get(FILE);
        File rootDir = taskContext.getRootDirectory();

        FileFinder finder = new FileFinder(rootDir);
        List<File> matching = finder.findFilesMatching(manifestName);
        if (matching.size() > 1) {
            throw new TaskException("Found " + matching.size()
                    + " files matching the pattern " + manifestName
                    + ". One matching file is expected.");
        } else if(matching.isEmpty()) {
            File parentDir =  new File(rootDir, manifestName).getParentFile();
            finder = new FileFinder(parentDir);
            String extension = Iterables.getLast(Lists.newArrayList(manifestName.split("\\.")), "yaml");
            List<File> similarMatches = finder.findFilesMatching(String.format("*.%s", extension));

            String message = "Found 0 files matching the pattern " + manifestName + ". ";

            if(!similarMatches.isEmpty()) {
                StringBuilder otherFiles = new StringBuilder();
                similarMatches.forEach(file -> otherFiles.append("\n").append(file.getName()));

                message = message + ". These files were found in the same directory: " + otherFiles.toString();
            }

            taskContext.getBuildLogger().addErrorLogEntry(message);
            throw new TaskException(message);
        }

        File manifestFile = matching.get(0);
        taskContext.getBuildLogger().addBuildLogEntry(
                "Loading service configuration from " + manifestFile.getAbsolutePath());
        InputStream input = null;
        try {
            input = new FileInputStream(manifestFile);
        } catch (FileNotFoundException e) {
            String message = "Manifest file not found: "
                    + manifestFile.getAbsolutePath();
            taskContext.getBuildLogger().addErrorLogEntry(message, e);
            throw new TaskException(message, e);
        }
        return input;
    }

}