/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.admin.actions;

import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.google.common.collect.Lists;
import org.cloudfoundry.client.v2.info.GetInfoResponse;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.admin.Credentials;
import org.gaptap.bamboo.cloudfoundry.admin.Proxy;
import org.gaptap.bamboo.cloudfoundry.admin.Target;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceFactory;
import org.gaptap.bamboo.cloudfoundry.client.ConnectionParameters;
import org.gaptap.bamboo.cloudfoundry.client.Log4jLogger;

import java.util.concurrent.CountDownLatch;

/**
 * @author David Ehringer
 * 
 */
@SuppressWarnings("serial")
public class ViewTargetAction extends BambooActionSupport {

	private int targetId;
	private Target target;
	private GetInfoResponse info;

	private String status;

	private final CloudFoundryAdminService adminService;
	private final CloudFoundryServiceFactory cloudFoundryClientFactory;

	public ViewTargetAction(CloudFoundryAdminService adminService, CloudFoundryServiceFactory cloudFoundryClientFactory) {
		this.adminService = adminService;
		this.cloudFoundryClientFactory = cloudFoundryClientFactory;
	}

	public String doView() throws InterruptedException {

		target = adminService.getTarget(targetId);
		if (target == null) {
			addActionError(getText("cloudfoundry.target.notFound", Lists.newArrayList(targetId)));
			return ERROR;
		}

		ConnectionParameters connectionParameters = buildConnectionParams();
		CloudFoundryService client = null;
		try {
			client = cloudFoundryClientFactory.getCloudFoundryService(connectionParameters, new Log4jLogger());
		} catch (Exception e) {
			addActionError(e.getMessage());
			return ERROR;
		}

		status = SUCCESS;
		CountDownLatch latch = new CountDownLatch(1);
		client
				.info()
				.subscribe(infoResponse -> {
							info = infoResponse;
						},
						throwable -> {
							addActionError(throwable.getMessage());
							status = ERROR;
							latch.countDown();
						},
						latch::countDown
		);
		latch.await();

		return status;
	}

	private ConnectionParameters buildConnectionParams() {
		// Warning, there is very similar code in ManageTargetsAction that should always be
		// updated when this code is updated. Consider refactoring.
		ConnectionParameters.Builder connectionParameters = ConnectionParameters.builder()
				.targetUrl(target.getUrl())
				.isTrustSelfSignedCerts(target.isTrustSelfSignedCerts());

		Credentials credentials = target.getCredentials();
		connectionParameters.username(credentials.getUsername());
		connectionParameters.password(credentials.getPassword());

		Proxy proxy = target.getProxy();
		if (proxy != null) {
			connectionParameters.proxyHost(proxy.getHost());
			connectionParameters.proxyPort(proxy.getPort());
		}
		return connectionParameters.build();
	}

	public int getTargetId() {
		return targetId;
	}

	public void setTargetId(int targetId) {
		this.targetId = targetId;
	}

	public Target getTarget() {
		return target;
	}

	public void setTarget(Target target) {
		this.target = target;
	}

	public GetInfoResponse getInfo() {
		return info;
	}

	public void setInfo(GetInfoResponse info) {
		this.info = info;
	}

}
