package org.gaptap.bamboo.cloudfoundry.client;

import org.cloudfoundry.reactor.util.DefaultSslCertificateTruster;
import org.cloudfoundry.reactor.util.SslCertificateTruster;
import org.cloudfoundry.reactor.util.StaticTrustManagerFactory;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

import java.time.Duration;
import java.util.Optional;
import reactor.netty.resources.LoopResources;

public class ReactorHealthChecker implements HealthChecker {

    private HttpClient client;
    private final SslCertificateTruster truster;

    public ReactorHealthChecker() {
        truster =  new DefaultSslCertificateTruster(Optional.empty(),
                                                    LoopResources.create("cloudfoundry-client", LoopResources.DEFAULT_IO_WORKER_COUNT, true));
        client = HttpClient.create();
    }

    public void setHttpClient(HttpClient httpClient) {
        this.client = httpClient;
    }

    @Override
    public Mono<Void> assertHealthy(ApplicationConfiguration applicationConfiguration, BlueGreenConfiguration blueGreenConfiguration, Logger logger) {
        assertRoutesMapped(applicationConfiguration);

        UriComponents uriComponents = getUriComponents(applicationConfiguration, blueGreenConfiguration);

        configureSsl(blueGreenConfiguration, uriComponents);

        return client
                .get()
                .uri(uriComponents.toString())
                .responseSingle((res, content) -> content.asString()
                        .map(it -> new ClientResponse(res.status().code(), it)))
                .doOnSubscribe(subscription -> logger.info("Checking health of " + uriComponents.toUriString()))
                .doOnError(throwable -> logger.error("Health check failed: " + throwable.getMessage()))
                .map(httpClientResponse -> assertSuccess(httpClientResponse, logger))
                .then();
    }

    protected UriComponents getUriComponents(ApplicationConfiguration applicationConfiguration, BlueGreenConfiguration blueGreenConfiguration) {
        UriComponents originalUri = UriComponentsBuilder
                .fromUriString("//" + applicationConfiguration.routes().get(0))
                .build();

        return UriComponentsBuilder.newInstance()
                .scheme("https")
                .host(originalUri.getHost())
                .port(443)
                .path(originalUri.getPath())
                .path(blueGreenConfiguration.healthCheckEndpoint())
                .build();
    }

    private void assertRoutesMapped(ApplicationConfiguration applicationConfiguration) {
        if(applicationConfiguration.routes().isEmpty()){
            throw new IllegalArgumentException("A health check cannot be performed on an application with no mapped routes.");
        }
    }

    private void configureSsl(BlueGreenConfiguration blueGreenConfiguration, UriComponents uriComponents) {
        if(blueGreenConfiguration.skipSslValidation()) {
            truster.trust(uriComponents.getHost(), uriComponents.getPort(), Duration.ofSeconds(30));
        }
    }

    private int assertSuccess(ClientResponse httpClientResponse, Logger logger){
        int responseCode = httpClientResponse.getStatus();
        if(200 != responseCode){
            logger.error("Health check failed. HTTP response code: " + responseCode);
            logger.error(httpClientResponse.getResponse());
            throw new HealthCheckException();
        }
        logger.info("Health check succeeded.");
        return responseCode;
    }
}
