package org.gaptap.bamboo.cloudfoundry.client;

import org.gaptap.bamboo.cloudfoundry.Nullable;
import org.immutables.value.Value;

import java.util.List;
import java.util.Map;

@Value.Immutable
abstract class _ApplicationConfiguration {

    public static final int DEFAULT_DISK_QUOTA = 1024;
    public static final Integer PLATFORM_DEFAULT_TIMEOUT = null;
    public static final int DEFAULT_INSTANCE_COUNT = 1;
    public static final Integer DEFAULT_MEMORY = 1024;

    @Value.Check
    void checkNoHostname(){
        if(noHostname() && !hosts().isEmpty()){
            throw new IllegalArgumentException("Invalid application configuration. Application must not be configured with both 'host'/'hosts' and 'no-hostname'");
        }
        if(noHostname() && !routes().isEmpty()){
            throw new IllegalArgumentException("Invalid application configuration. Application must not be configured with both 'routes' and 'no-hostname'");
        }
    }

    @Value.Check
    void checkRoutes(){
        if(!routes().isEmpty() && !hosts().isEmpty()){
            throw new IllegalArgumentException("Invalid application configuration. Application must not be configured with both 'routes' and 'host'/'hosts'");
        }
        if(!routes().isEmpty() && !domains().isEmpty()){
            throw new IllegalArgumentException("Invalid application configuration. Application must not be configured with both 'routes' and 'domain'/'domains'");
        }
    }

    abstract String name();

    @Nullable
    abstract Integer memory();

    @Nullable
    abstract Integer diskQuota();

    @Nullable
    abstract Integer instances();

    @Nullable
    abstract String command();

    abstract List<String> buildpacks();

    @Nullable
    abstract String stack();

    abstract Map<String, String> environment();

    @Value.Default
    public boolean noHostname() {
        return false;
    }

    abstract List<String> hosts();

    abstract List<String> domains();

    abstract List<String> routes();

    @Nullable
    abstract Integer healthCheckTimeout();

    abstract List<String> serviceBindings();

    @Nullable
    abstract String healthCheckType();

    @Nullable
    abstract String healthCheckHttpEndpoint();

    @Nullable
    abstract Boolean noRoute();

    public void logConfiguration(Logger logger){
        logger.info("--- Specified Application Configuration ---");
        logger.info("name: " + name());
        logger.info("instances: " + valueOrEmpty(instances()));
        logger.info("memory: " + valueOrEmpty(memory()) + "M");
        logger.info("diskQuota: " + valueOrEmpty(diskQuota()) + "M");
        logger.info("buildpacks: " + valueOrEmpty(buildpacks()));
        logger.info("stack: " + valueOrEmpty(stack()));
        logger.info("health check type: " + valueOrEmpty(healthCheckType()));
        logger.info("health check http endpoint: " + valueOrEmpty(healthCheckHttpEndpoint()));
        logger.info("health check timeout: " + valueOrEmpty(healthCheckTimeout()));
        logger.info("routes: " + valueOrEmpty(routes()));
        logger.info("domains: " + valueOrEmpty(domains()));
        logger.info("hosts: " + valueOrEmpty(hosts()));
        logger.info("no-hostname: " + valueOrEmpty(noHostname()));
        logger.info("no-route: " + valueOrEmpty(noRoute()));
        logger.info("bound services: " + valueOrEmpty(serviceBindings()));
        logger.info("environment: ");
        for(Map.Entry entry: environment().entrySet()){
            logger.info("  - " + entry.getKey() + "=" + "[REDACTED]");
        }
        logger.info("-------------------------------------------");
    }

    private void list(List<String> list, Logger logger) {
        for(String entry: list){
            logger.info(" - " + entry);
        }
    }

    private String valueOrEmpty(Object field) {
        return field == null ? "" :String.valueOf(field);
    }

}
