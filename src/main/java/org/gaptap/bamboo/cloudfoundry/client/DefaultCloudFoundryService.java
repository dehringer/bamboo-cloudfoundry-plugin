/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import org.cloudfoundry.client.CloudFoundryClient;
import org.cloudfoundry.client.v2.domains.DeleteDomainRequest;
import org.cloudfoundry.client.v2.info.GetInfoRequest;
import org.cloudfoundry.client.v2.info.GetInfoResponse;
import org.cloudfoundry.client.v2.organizations.ListOrganizationPrivateDomainsRequest;
import org.cloudfoundry.client.v2.privatedomains.PrivateDomainResource;
import org.cloudfoundry.client.v2.routes.ListRoutesRequest;
import org.cloudfoundry.client.v2.routes.RouteResource;
import org.cloudfoundry.client.v2.serviceinstances.BindServiceInstanceRouteRequest;
import org.cloudfoundry.client.v2.services.ListServicesRequest;
import org.cloudfoundry.client.v2.services.ServiceEntity;
import org.cloudfoundry.client.v2.services.ServiceResource;
import org.cloudfoundry.client.v2.shareddomains.ListSharedDomainsRequest;
import org.cloudfoundry.client.v2.shareddomains.SharedDomainResource;
import org.cloudfoundry.client.v3.tasks.CancelTaskRequest;
import org.cloudfoundry.client.v3.tasks.CreateTaskRequest;
import org.cloudfoundry.client.v3.tasks.CreateTaskResponse;
import org.cloudfoundry.client.v3.tasks.GetTaskRequest;
import org.cloudfoundry.client.v3.tasks.GetTaskResponse;
import org.cloudfoundry.client.v3.tasks.TaskState;
import org.cloudfoundry.doppler.DopplerClient;
import org.cloudfoundry.operations.CloudFoundryOperations;
import org.cloudfoundry.operations.applications.ApplicationDetail;
import org.cloudfoundry.operations.applications.ApplicationSummary;
import org.cloudfoundry.operations.applications.DeleteApplicationRequest;
import org.cloudfoundry.operations.applications.GetApplicationRequest;
import org.cloudfoundry.operations.applications.RenameApplicationRequest;
import org.cloudfoundry.operations.applications.RestartApplicationRequest;
import org.cloudfoundry.operations.applications.StartApplicationRequest;
import org.cloudfoundry.operations.applications.StopApplicationRequest;
import org.cloudfoundry.operations.domains.CreateDomainRequest;
import org.cloudfoundry.operations.organizations.OrganizationInfoRequest;
import org.cloudfoundry.operations.routes.CreateRouteRequest;
import org.cloudfoundry.operations.routes.DeleteRouteRequest;
import org.cloudfoundry.operations.routes.MapRouteRequest;
import org.cloudfoundry.operations.routes.UnmapRouteRequest;
import org.cloudfoundry.operations.services.BindServiceInstanceRequest;
import org.cloudfoundry.operations.services.CreateServiceInstanceRequest;
import org.cloudfoundry.operations.services.CreateUserProvidedServiceInstanceRequest;
import org.cloudfoundry.operations.services.DeleteServiceInstanceRequest;
import org.cloudfoundry.operations.services.GetServiceInstanceRequest;
import org.cloudfoundry.operations.services.ServiceInstance;
import org.cloudfoundry.operations.services.ServiceInstanceSummary;
import org.cloudfoundry.operations.services.UnbindServiceInstanceRequest;
import org.cloudfoundry.operations.services.UpdateServiceInstanceRequest;
import org.cloudfoundry.util.DelayTimeoutException;
import org.cloudfoundry.util.ExceptionUtils;
import org.cloudfoundry.util.PaginationUtils;
import org.cloudfoundry.util.ResourceUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple3;

import java.time.Duration;
import java.util.Collections;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

import static org.cloudfoundry.util.DelayUtils.exponentialBackOff;
import static org.cloudfoundry.util.tuple.TupleUtils.function;

/**
 * @author David Ehringer
 * 
 */
public class DefaultCloudFoundryService implements CloudFoundryService {

    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(DefaultCloudFoundryService.class);

    private final CloudFoundryOperations cloudFoundryOperations;
    private final CloudFoundryClient cloudFoundryClient;
    private final DopplerClient dopplerClient;

    private final Logger logger;
    private final HealthChecker healthChecker;

    public DefaultCloudFoundryService(CloudFoundryOperations cloudFoundryOperations, CloudFoundryClient cloudFoundryClient,
                                      DopplerClient dopplerClient, Logger logger, HealthChecker healthChecker) {
        this.cloudFoundryOperations = cloudFoundryOperations;
        this.cloudFoundryClient = cloudFoundryClient;
        this.dopplerClient = dopplerClient;
        this.logger = logger;
        this.healthChecker = healthChecker;
    }

    @Override
    public Mono<Void> validateConnection() {
        return cloudFoundryOperations
                .buildpacks()
                .list()
                .then();
    }

    @Override
    public Mono<GetInfoResponse> info() {
        return cloudFoundryClient
                .info()
                .get(GetInfoRequest.builder()
                        .build())
                .doOnSubscribe(subscription -> logger.info("Getting CloudInfo..."))
                .doOnSuccess(response -> logger.info("Getting CloudInfo... OK"))
                .doOnError(throwable -> logger.info("Getting CloudInfo... FAILED: " + throwable.getMessage()));
    }

    @Override
    public Mono<String> defaultDomain(String organizationName) {
        return getSharedDomainNames()
                .switchIfEmpty(getPrivateDomainNames(organizationName))
                .next()
                .switchIfEmpty(ExceptionUtils.illegalArgument("A shared or private domain not found"));
    }

    private Flux<String> getSharedDomainNames() {
        return requestSharedDomains()
                .map(domain -> domain.getEntity().getName());
    }

    private Flux<SharedDomainResource> requestSharedDomains() {
        return PaginationUtils
                .requestClientV2Resources(page -> cloudFoundryClient.sharedDomains()
                        .list(ListSharedDomainsRequest.builder()
                                .page(page)
                                .build()));
    }

    private Flux<String> getPrivateDomainNames(String organizationName) {
        return cloudFoundryOperations
                .organizations()
                .get(OrganizationInfoRequest.builder()
                        .name(organizationName)
                        .build())
                .map(organizationDetail -> organizationDetail.getId())
                .flatMapMany(organizationId -> requestPrivateDomains(organizationId))
                .map(privateDomainResource -> privateDomainResource.getEntity().getName());
    }

    private Flux<PrivateDomainResource> requestPrivateDomains(String organizationId) {
        return PaginationUtils
                .requestClientV2Resources(page -> cloudFoundryClient.organizations()
                        .listPrivateDomains(ListOrganizationPrivateDomainsRequest.builder()
                                .organizationId(organizationId)
                                .page(page)
                                .build()));
    }

    @Override
    public Flux<ApplicationSummary> apps() {
        return cloudFoundryOperations
                .applications()
                .list()
                .doOnSubscribe(subscription -> logger.info("Getting applications..."))
                .doOnComplete(() -> logger.info("Getting applications.. OK"))
                .doOnError(throwable -> logger.error("Getting applications... FAILED: " + throwable.getMessage()));
    }

    @Override
    public Mono<ApplicationDetail> app(String name) {
        return getApp(name)
                .doOnSubscribe(it -> logger.info(String.format("Getting application %s...", name)))
                .doOnSuccess(app -> logger.info(String.format("Getting application %s... OK", name)))
                .doOnError(error -> logger.error(String.format("Getting application %s... FAILED: %s", name, error.getMessage())));
    }

    public Mono<ApplicationDetail> getApp(String name) {
        return cloudFoundryOperations
                .applications()
                .get(GetApplicationRequest.builder()
                        .name(name)
                        .build());
    }

    @Override
    public Mono<ApplicationDetail> push(ApplicationConfiguration applicationConfiguration, PushConfiguration pushConfiguration) {
        DeclarativePush declarativePush = new DeclarativePush(cloudFoundryOperations, cloudFoundryClient, dopplerClient, logger, this);
        return declarativePush.push(applicationConfiguration, pushConfiguration);
    }

    @Override
    public Mono<ApplicationDetail> push(ApplicationConfiguration applicationConfiguration, PushConfiguration pushConfiguration, BlueGreenConfiguration blueGreenConfiguration) {
        ApplicationConfiguration darkAppConfiguration = blueGreenConfiguration.convertToDarkAppConfiguration(applicationConfiguration);
        return  doDeleteIfAppExists(darkAppConfiguration)
                .then(push(darkAppConfiguration, pushConfiguration))
                .then(performOptionalHealthCheck(darkAppConfiguration, blueGreenConfiguration))
                .thenMany(mapLiveUrlsToDarkApp(darkAppConfiguration, applicationConfiguration))
                .thenMany(unMapDarkUrlsFromDarkApp(darkAppConfiguration))
                .thenEmpty(deleteOldApp(applicationConfiguration))
                .then(renameApp(darkAppConfiguration.name(), applicationConfiguration.name()))
                .then(app(applicationConfiguration.name()))
                .doOnSubscribe(subscription -> logger.info("Starting blue/green deployment"))
                .doOnSuccess(applicationDetail -> logger.info("Blue/green deployment successfully completed."))
                .doOnError(throwable -> {
                    logger.error("Blue/green deployment failed: " + throwable.getMessage());
                    LOG.error("Blue/green deployment failed: " + throwable.getMessage(), throwable);
                });
    }

    private Mono<Void> performOptionalHealthCheck(ApplicationConfiguration applicationConfiguration, BlueGreenConfiguration blueGreenConfiguration) {
        if(blueGreenConfiguration.healthCheckEnabled()){
            return healthChecker.assertHealthy(applicationConfiguration, blueGreenConfiguration, logger);
        }
        return Mono.empty();
    }

    private Flux<Void> mapLiveUrlsToDarkApp(ApplicationConfiguration darkApplicationConfiguration, ApplicationConfiguration applicationConfiguration) {
        return Flux.fromIterable(applicationConfiguration.routes())
                .flatMap(route -> map(darkApplicationConfiguration.name(), route), 1);
    }

    private Flux<Void> unMapDarkUrlsFromDarkApp(ApplicationConfiguration applicationConfiguration) {
        return unMapUrlsFromApp(applicationConfiguration);
    }

    private Flux<Void> unMapLiveUrlsFromOldApp(ApplicationConfiguration applicationConfiguration) {
        return unMapUrlsFromApp(applicationConfiguration);
    }

    private Flux<Void> unMapUrlsFromApp(ApplicationConfiguration applicationConfiguration) {
        return Flux.fromIterable(applicationConfiguration.routes())
                .flatMap(route -> Mono.zip(
                        Mono.just(route),
                        getApp(applicationConfiguration.name())))
                .flatMap(function((route, applicationDetail) -> unmap(applicationConfiguration.name(), route)), 1);
    }


    private Mono<Void> deleteOldApp(ApplicationConfiguration applicationConfiguration) {
        return deleteApp(applicationConfiguration.name());
    }

    @Override
    public Mono<Void> startApp(String name) {
        return startApp(name, null, null);
    }

    @Override
    public Mono<Void> startApp(String name, Integer startupTimeout, Integer stagingTimeout) {
        Optional<Duration> startupTimeoutDuration = Optional.ofNullable(startupTimeout).map(i -> Duration.ofSeconds(i));
        Optional<Duration> stagingTimeoutDuration = Optional.ofNullable(stagingTimeout).map(i -> Duration.ofSeconds(i));
        StartApplicationRequest.Builder builder = StartApplicationRequest.builder()
                .name(name);
        if(startupTimeoutDuration.isPresent()){
            builder.startupTimeout(startupTimeoutDuration.get());
        }
        if(stagingTimeoutDuration.isPresent()){
            builder.stagingTimeout(stagingTimeoutDuration.get());
        }
        return cloudFoundryOperations
                .applications()
                .start(builder.build())
                .doOnSubscribe(it -> logger.info(String.format("Starting application %s...", name)))
                .doOnSuccess(aVoid -> logger.info(String.format("Starting application %s... OK", name)))
                .doOnError(error -> logger.error(String.format("Starting application %s... FAILED: %s", name, error.getMessage())));
    }

    @Override
    public Mono<Void> stopApp(String name) {
        return cloudFoundryOperations
                .applications()
                .stop(StopApplicationRequest.builder()
                        .name(name)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Stopping application %s...", name)))
                .doOnSuccess(aVoid -> logger.info(String.format("Stopping application %s... OK", name)))
                .doOnError(error -> logger.error(String.format("Stopping application %s... FAILED: %s", name, error.getMessage())));
    }

    @Override
    public Mono<Void> restartApp(String name) {
        return cloudFoundryOperations
                .applications()
                .restart(RestartApplicationRequest.builder()
                        .name(name)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Restarting application %s...", name)))
                .doOnSuccess(aVoid -> logger.info(String.format("Restarting application %s... OK", name)))
                .doOnError(error -> logger.error(String.format("Restarting application %s... FAILED: %s", name, error.getMessage())));
    }

    @Override
    public Mono<Void> deleteApp(String name) {
        return getApp(name)
                .flatMap(app -> doDelete(name))
                .doOnSubscribe(subscription -> logger.info(String.format("Deleting application %s...", name)))
                .onErrorResume(Exception.class, e -> {
                    logger.info(String.format("Application %s does not exist", name));
                    return Mono.empty();
                })
                .doOnSuccess(aVoid -> logger.info(String.format("Deleting application %s... OK", name)));
    }

    private Mono<Void> doDelete(String name) {
        return cloudFoundryOperations
                .applications()
                .delete(DeleteApplicationRequest.builder()
                        .name(name)
                        .build())
                .doOnError(error -> logger.error(String.format("Deleting application %s... FAILED: %s", name, error.getMessage())));
    }

    private Mono<Void> doDeleteIfAppExists(ApplicationConfiguration applicationConfiguration) {
        return cloudFoundryOperations
                .applications()
                .delete(DeleteApplicationRequest.builder()
                        .name(applicationConfiguration.name())
                        .build())
                .doOnSubscribe(subscription -> LOG.debug(String.format("Deleting application %s...", applicationConfiguration.name())))
                .onErrorResume( error -> {
                    LOG.debug(String.format("Deleting application %s... FAILED: %s", applicationConfiguration.name(), error.getMessage()));
                    return Mono.empty();
                })
                .doOnSuccess(aVoid -> LOG.debug(String.format("Deleting application %s... OK", applicationConfiguration.name())));
    }

    @Override
    public Mono<Void> map(String appName, String uri) {
        return map(RouteRequestBuilder.buildMapRouteRequest(appName, uri));
    }

    @Override
    public Mono<Void> map(MapRouteRequest request){
        String uri = RouteRequestBuilder.toString(request);
        String appName = request.getApplicationName();
        return cloudFoundryOperations
                .routes()
                .map(request)
                .doOnSubscribe(it -> logger.info(String.format("Mapping URL %s to application %s...", uri, appName)))
                .doOnSuccess(aVoid -> logger.info(String.format("Mapping URL %s to application %s... OK", uri, appName)))
                .doOnError(error -> logger.error(String.format("\"Mapping URL %s to application %s... FAILED: %s", uri, appName, error.getMessage())))
                .then();
    }

    @Override
    public Mono<Void> unmap(String appName, String uri) {
        return unmap(RouteRequestBuilder.buildUnmapRouteRequest(appName, uri));
    }

    @Override
    public Mono<Void> unmap(UnmapRouteRequest request) {
        String uri = RouteRequestBuilder.toString(request);
        String appName = request.getApplicationName();
        return cloudFoundryOperations
                .routes()
                .unmap(request)
                .doOnSubscribe(subscription -> logger.info(String.format("Unmapping URL %s from application %s...", uri, appName)))
                .doOnSuccess(aVoid -> logger.info(String.format("Unmapping URL %s from application %s... OK", uri, appName)))
                .doOnError(error -> logger.error(String.format("Unmapping URL %s from application %s... FAILED: %s", uri, appName, error.getMessage())));
    }

    public Mono<Void> renameApp(String appName, String newName) {
        return renameApp(appName, newName, true);
    }

    @Override
    public Mono<Void> renameApp(String appName, String newName, boolean failIfAppDoesNotExist) {
        return getApp(appName)
                .then(doRenameApp(appName, newName))
                .onErrorResume(e -> {
                    if(failIfAppDoesNotExist){
                        return Mono.error(e);
                    }
                    logger.info(String.format("Application %s does not exist. No rename will occur.", appName));
                    return Mono.empty();
                });
    }

    private Mono<Void> doRenameApp(String appName, String newName) {
        return cloudFoundryOperations
                .applications()
                .rename(RenameApplicationRequest.builder()
                        .name(appName)
                        .newName(newName)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Renaming application %s to %s...", appName, newName)))
                .doOnSuccess(aVoid -> logger.info(String.format("Renaming application %s to %s... OK", appName, newName)))
                .doOnError(error -> logger.error(String.format("Renaming application %s to %s... FAILED: %s", appName, newName, error.getMessage())));
    }

    @Override
    public Flux<ServiceInstanceSummary> services() {
        return cloudFoundryOperations
                .services()
                .listInstances()
                .doOnSubscribe(subscription -> logger.info("Getting services.."))
                .doOnComplete(() -> logger.info("Getting services.. OK"))
                .doOnError(throwable -> logger.error("Getting services... FAILED: " + throwable.getMessage()));
    }

    @Override
    public Mono<ServiceInstance> service(String name) {
        return getService(name)
                .doOnSubscribe(subscription -> logger.info(String.format("Getting service %s...", name)))
                .doOnSuccess(instance -> logger.info(String.format("Getting service %s... OK", name)))
                .doOnError(error -> logger.error(String.format("Getting service %s... FAILED: %s", name, error.getMessage())));
    }

    public Mono<ServiceInstance> getService(String name) {
        return cloudFoundryOperations
                .services()
                .getInstance(GetServiceInstanceRequest.builder()
                        .name(name)
                        .build());
    }

    @Override
    public Mono<Void> createService(String serviceInstanceName, String service, String plan, boolean failIfExists) {
        if(!failIfExists){
            return service(serviceInstanceName)
                    .doOnNext(instance -> logger.info(String.format("Service named %s already exists. No action will be taken.", serviceInstanceName)))
                    .then()
                    .onErrorResume(Exception.class, t -> createService(serviceInstanceName, service, plan));
        }
        return createService(serviceInstanceName, service, plan);
    }

    public Mono<Void> createService(String serviceInstanceName, String service, String plan) {
        ServiceConfiguration serviceConfiguration = ServiceConfiguration.builder()
                .serviceInstance(serviceInstanceName)
                .service(service)
                .plan(plan)
                .build();
        return createService(serviceConfiguration);
    }

    private Mono<Void> createService(ServiceConfiguration serviceConfiguration) {
        return cloudFoundryOperations
                .services()
                .createInstance(CreateServiceInstanceRequest.builder()
                        .serviceName(serviceConfiguration.getService())
                        .planName(serviceConfiguration.getPlan())
                        .serviceInstanceName(serviceConfiguration.getServiceInstance())
                        .parameters(serviceConfiguration.getParameters())
                        .tags(serviceConfiguration.getTags())
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Creating service %s...", serviceConfiguration.getServiceInstance())))
                .doOnSuccess(instance -> logger.info(String.format("Creating service %s... OK", serviceConfiguration.getServiceInstance())))
                .doOnError(error -> logger.error(String.format("Creating service %s... FAILED: %s", serviceConfiguration.getServiceInstance(), error.getMessage())));
    }

    public Mono<Void> updateService(ServiceConfiguration serviceConfiguration){
        return getService(serviceConfiguration.getServiceInstance())
                .flatMap(serviceInstance -> Mono.zip(
                        Mono.just(serviceConfiguration),
                        Mono.just(serviceInstance),
                        requestListServices()
                                .filter(serviceResource -> serviceResource.getEntity().getLabel().equals(serviceInstance.getService()))
                                .single()
                ))
                .flatMapMany(tuple -> getUpdateServiceInstanceRequest(tuple))
                .single()
                .flatMap(updateServiceInstanceRequest -> cloudFoundryOperations
                        .services()
                        .updateInstance(updateServiceInstanceRequest))
                .doOnSubscribe(subscription -> logger.info(String.format("Updating service %s...", serviceConfiguration.getServiceInstance())))
                .doOnSuccess(service -> logger.info(String.format("Updating service %s... OK", serviceConfiguration.getServiceInstance())))
                .doOnError(error -> logger.error(String.format("Updating service %s... FAILED: %s", serviceConfiguration.getServiceInstance(), error.getMessage())));
    }

    private Flux<ServiceResource> requestListServices() {
        return PaginationUtils
                .requestClientV2Resources(page -> cloudFoundryClient
                        .services()
                        .list(ListServicesRequest.builder()
                                .build()));
    }

    private Mono<UpdateServiceInstanceRequest> getUpdateServiceInstanceRequest(Tuple3<ServiceConfiguration, ServiceInstance, ServiceResource> tuple) {
        return getUpdateServiceInstanceRequest(tuple.getT1(), tuple.getT2(), tuple.getT3().getEntity());
    }

    private Mono<UpdateServiceInstanceRequest> getUpdateServiceInstanceRequest(ServiceConfiguration serviceConfiguration, ServiceInstance serviceInstance,
                                                                         ServiceEntity serviceEntity) {
        if(serviceEntity.getPlanUpdateable()){
            return Mono.just(UpdateServiceInstanceRequest.builder()
                    .serviceInstanceName(serviceConfiguration.getServiceInstance())
                    .addAllTags(serviceConfiguration.getTags())
                    .parameters(serviceConfiguration.getParameters())
                    .planName(serviceConfiguration.getPlan())
                    .build());
        }
        String currentPlan = serviceInstance.getPlan();
        String requestedPlan = serviceConfiguration.getPlan();
        if(!currentPlan.equals(requestedPlan)){
            return Mono.error(new CloudFoundryServiceException(
                    String.format("A plan change from %s to %s was requested but the %s service does not support updating of plans",
                            currentPlan, requestedPlan, serviceEntity.getLabel())));
        }
        return Mono.just(UpdateServiceInstanceRequest.builder()
                .serviceInstanceName(serviceConfiguration.getServiceInstance())
                .addAllTags(serviceConfiguration.getTags())
                .parameters(serviceConfiguration.getParameters())
                .build());
    }

    @Override
    public Mono<Void> createUserProvidedService(String name, Map<String, Object> credentials, String syslogDrainUrl, String routeServiceUrl, boolean ignoreIfExists) {
        if(ignoreIfExists){
            return getService(name)
                    .doOnNext(instance -> logger.info(String.format("Service named %s already exists. No action will be taken.", name)))
                    .then()
                    .onErrorResume(Exception.class, t -> createUserProvidedService(name, credentials, syslogDrainUrl, routeServiceUrl));
        }
        return createUserProvidedService(name, credentials, syslogDrainUrl, routeServiceUrl);
    }

    public Mono<Void> createUserProvidedService(String name, Map<String, Object> credentials, String syslogDrainUrl, String routeServiceUrl) {
        return cloudFoundryOperations
                .services()
                .createUserProvidedInstance(CreateUserProvidedServiceInstanceRequest.builder()
                        .name(name)
                        .credentials(credentials)
                        .syslogDrainUrl(syslogDrainUrl)
                        .routeServiceUrl(routeServiceUrl)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Creating user provided service %s...", name)))
                .doOnSuccess(service -> logger.info(String.format("Creating user provided service %s... OK", name)))
                .doOnError(error -> logger.error(String.format("Creating user provided service %s... FAILED: %s", name, error.getMessage())));
    }

    @Override
    public Mono<Void> pushService(ServiceConfiguration serviceConfiguration) {
        return Mono.empty()
                .then(getService(serviceConfiguration.getServiceInstance())
                        .then(updateService(serviceConfiguration)))
                .onErrorResume(IllegalArgumentException.class, t -> createService(serviceConfiguration));
    }

    @Override
    public Mono<Void> deleteService(String name) {
        return cloudFoundryOperations
                .services()
                .deleteInstance(DeleteServiceInstanceRequest.builder()
                        .name(name)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Deleting service %s...", name)))
                .doOnSuccess(it -> logger.info(String.format("Deleting service %s... OK", name)))
                .doOnError(error -> logger.error(String.format("Deleting service %s... FAILED: %s", name, error.getMessage())));
    }

    @Override
    public Mono<Void> bindService(String serviceName, String applicationName) {
        return cloudFoundryOperations
                .services()
                .bind(BindServiceInstanceRequest.builder()
                        .serviceInstanceName(serviceName)
                        .applicationName(applicationName)
                        .build())
                .doOnSubscribe(it -> logger.info(String.format("Binding service %s to app %s...", serviceName, applicationName)))
                .doOnSuccess(it -> logger.info(String.format("Binding service %s to app %s... OK", serviceName, applicationName)))
                .doOnError(error -> logger.error(String.format("Binding service %s to app %s... FAILED: %s", serviceName, applicationName, error.getMessage())));
    }

    @Override
    public Mono<Void> unbindService(String serviceName, String applicationName) {
        return cloudFoundryOperations
                .services()
                .unbind(UnbindServiceInstanceRequest.builder()
                        .serviceInstanceName(serviceName)
                        .applicationName(applicationName)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Unbinding service %s from app %s...", serviceName, applicationName)))
                .doOnSuccess(it -> logger.info(String.format("Unbinding service %s from app %s... OK", serviceName, applicationName)))
                .doOnError(error -> logger.error(String.format("Unbinding service %s from app %s... FAILED: %s", serviceName, applicationName, error.getMessage())));
    }

    @Override
    public Mono<Void> addDomain(String domain, String organization) {
        return cloudFoundryOperations
                .domains()
                .create(CreateDomainRequest.builder()
                        .domain(domain)
                        .organization(organization)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Adding private domain %s to organization %s...", domain, organization)))
                .doOnSuccess(it -> logger.info(String.format("Adding private domain %s to organization %s... OK", domain, organization)))
                .doOnError(error -> logger.error(String.format("Adding private domain %s to organization %s.... FAILED: %s", domain, organization, error.getMessage())));
    }

    @Override
    public Mono<Void> deleteDomain(String domainName) {
        return getDomainId(domainName)
                .switchIfEmpty(Mono.error(new IllegalArgumentException("Domain " + domainName + " does not exist.")))
                .flatMap(domainId -> doDeleteDomain(domainId))
                .doOnSubscribe(subscription -> logger.info(String.format("Deleting domain %s...", domainName)))
                .doOnSuccess(it -> logger.info(String.format("Deleting domain %s... OK", domainName)))
                .doOnError(error -> logger.error(String.format("Deleting domain %s.... FAILED: %s", domainName, error.getMessage())));
    }

    private Mono<String> getDomainId(String domainName){
        return cloudFoundryOperations
                .domains()
                .list()
                .filter(domain -> domain.getName().equals(domainName))
                .map(d -> d.getId())
                .singleOrEmpty();
    }

    private Mono<Void> doDeleteDomain(String domainId){
        return cloudFoundryClient
                .domains()
                .delete(DeleteDomainRequest.builder()
                        .domainId(domainId)
                        .build())
                .then();
    }

    @Override
    public Mono<Void> createRoute(String domain, String host, String path, String space) {
        return cloudFoundryOperations
                .routes()
                .create(CreateRouteRequest.builder()
                        .domain(domain)
                        .host(host)
                        .path(path)
                        .space(space)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Adding route %s.%s%s...", host, domain, path)))
                .doOnSuccess(it -> logger.info(String.format("Adding route %s.%s%s... OK", host, domain, path)))
                .doOnError(error -> logger.error(String.format("Adding route %s.%s%s... FAILED: %s", host, domain, path, error.getMessage())))
                .then();
    }

    @Override
    public Mono<Void> deleteRoute(String domain, String host, String path) {
        return cloudFoundryOperations
                .routes()
                .delete(DeleteRouteRequest.builder()
                        .domain(domain)
                        .host(host)
                        .path(path)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Deleting route %s.%s%s...", host, domain, path)))
                .doOnSuccess(it -> logger.info(String.format("Deleting route %s.%s%s... OK", host, domain, path)))
                .doOnError(error -> logger.error(String.format("Deleting route %s.%s%s... FAILED: %s", host, domain, path, error.getMessage())));
    }

    @Override
    public Mono<Void> bindRouteService(String serviceInstanceName, String domain, String host, String path, Map<String, Object> jsonParameters) {
        return getServiceInstanceId(serviceInstanceName)
                .flatMap(serviceInstanceId -> Mono.zip(
                        Mono.just(serviceInstanceId),
                        getDomainId(domain)
                ))
                .flatMap(function((serviceInstanceId, domainId) -> Mono.zip(
                        Mono.just(serviceInstanceId),
                        getRouteId(domainId, host, path)
                )))
                .flatMap(function((serviceInstanceId, routeId) -> requestBindServiceToRoute(serviceInstanceId, routeId, jsonParameters)))
                .doOnSuccess(it -> logger.info(String.format("Successfully bound service to route.")))
                .onErrorResume(routeServiceAlreadyBound(), handleRouteServiceAlreadyBound());
    }

    private Predicate<? super Throwable> routeServiceAlreadyBound(){
        return e -> e.getMessage().contains("CF-ServiceInstanceAlreadyBoundToSameRoute");
    }

    private Function<? super Throwable, Mono<Void>> handleRouteServiceAlreadyBound(){
        return e -> Mono
                .empty()
                .doOnSubscribe(subscription -> logger.warn("The service instance is already bound to the route. No action will be taken."))
                .then();
    }

    private Mono<Void> requestBindServiceToRoute(String serviceInstanceId, String routeId, Map<String, Object> jsonParameters){
        return cloudFoundryClient
                .serviceInstances()
                .bindRoute(BindServiceInstanceRouteRequest.builder()
                        .serviceInstanceId(serviceInstanceId)
                        .routeId(routeId)
                        .parameters(jsonParameters)
                        .build())
                .then();
    }

    private Mono<String> getServiceInstanceId(String serviceInstanceName){
        return cloudFoundryOperations
                .services()
                .getInstance(GetServiceInstanceRequest.builder()
                        .name(serviceInstanceName)
                        .build())
                .map(serviceInstance -> serviceInstance.getId())
                .switchIfEmpty(ExceptionUtils.illegalArgument("Service instance %s does not exist", serviceInstanceName));
    }

    private Mono<String> getRouteId(String domainId, String host, String path){
        return getRoute(domainId, host, path)
                .map(ResourceUtils::getId);
    }

    private Mono<RouteResource> getRoute(String domainId, String host, String path) {
        return requestRoutes(domainId, host, path)
                .filter(resource -> isIdentical(host, ResourceUtils.getEntity(resource).getHost()))
                .filter(resource -> isIdentical(path, ResourceUtils.getEntity(resource).getPath()))
                .single()
                .onErrorResume(NoSuchElementException.class, e -> ExceptionUtils.illegalArgument("Specified route does not exist."));
    }

    private Flux<RouteResource> requestRoutes(String domainId, String host, String path) {
        return requestRoutes(builder -> builder
                        .domainId(domainId)
                        .hosts(Optional.ofNullable(host).map(Collections::singletonList).orElse(null))
                        .paths(Optional.ofNullable(path).map(Collections::singletonList).orElse(null))
        );
    }

    private Flux<RouteResource> requestRoutes(UnaryOperator<ListRoutesRequest.Builder> modifier) {
        ListRoutesRequest.Builder listBuilder = modifier.apply(ListRoutesRequest.builder());
        return PaginationUtils
                .requestClientV2Resources(page -> cloudFoundryClient.routes()
                        .list(listBuilder
                                .page(page)
                                .build()));
    }

    private static boolean isIdentical(String s, String t) {
        return s == null ? t == null : s.equals(t);
    }

    @Override
    public Mono<CreateTaskResponse> runTask(String applicationName, String command, String taskName, Integer memory, Map<String, String> environment) {
        //TODO Add disk and potentially other options when reviewing new GA documentation
        return cloudFoundryOperations
                .applications()
                .get(GetApplicationRequest.builder()
                        .name(applicationName)
                        .build())
                .map(applicationDetail -> applicationDetail.getId())
                .flatMap(applicationId -> cloudFoundryClient
                        .tasks()
                        .create(CreateTaskRequest.builder()
                                .applicationId(applicationId)
                                .name(taskName)
                                .command(command)
                                .memoryInMb(memory)
                                .build()))
                .doOnSuccess(it -> logger.info(String.format("Task %s has been submitted successfully for execution.", taskName)))
                .doOnError(error -> logger.error(String.format("Creating task for app %s... FAILED: %s", applicationName, error.getMessage())));
    }

    @Override
    public Mono<Void> waitForTaskCompletion(String applicationName, String taskId, int timeout, boolean terminateTaskOnTimeout) {
        AppLogManager logManager = new AppLogManager(cloudFoundryOperations, dopplerClient);
        return Mono
                .just(applicationName)
                .doOnNext(appName -> logManager.startTailingLogs(appName, logger))
                .then(getTask(taskId)
                        .filter(isComplete())
                        .repeatWhenEmpty(exponentialBackOff(Duration.ofSeconds(1), Duration.ofSeconds(15), Duration.ofSeconds(timeout)))
                        .doOnNext(task ->  logger.info(String.format("Task %s state: %s", task.getName(), task.getState())))
                        .doOnNext(task -> logManager.stopTailingLogs())
                        .filter(isSuccessful())
                        .switchIfEmpty(ExceptionUtils.illegalState("One-off task %s did not complete successfully.", taskId))
                        .onErrorResume(DelayTimeoutException.class, t -> handleTaskTimeout(taskId, terminateTaskOnTimeout, logManager))
                        .then());
    }

    private <T> Mono<T> handleTaskTimeout(String taskId, boolean terminateTaskOnTimeout, AppLogManager logManager){
        return (Mono<T>) Mono
                .just(terminateTaskOnTimeout)
                .doOnNext(aBoolean -> logManager.stopTailingLogs())
                .doOnSubscribe(subscription -> logger.error(String.format("One-off task %s timed out", taskId)))
                .filter(terminate -> terminate)
                .map(terminate -> taskId)
                .flatMap(this::terminateTask)
                .thenEmpty(ExceptionUtils.illegalState("One-off task %s timed out.", taskId))
                .switchIfEmpty(ExceptionUtils.illegalState("One-off task %s timed out.", taskId));
    }

    private Mono<Void> terminateTask(String taskId){
        return cloudFoundryClient
                .tasks()
                .cancel(CancelTaskRequest.builder()
                        .taskId(taskId)
                        .build())
                .doOnSubscribe(subscription -> logger.error(String.format("Terminating task %s...", taskId)))
                .doOnSuccess(cancelTaskResponse -> logger.error(String.format("Terminating task %s...OK", taskId)))
                .onErrorResume(Exception.class, e -> {
                    logger.error(String.format("Terminating task %s...FAILED", taskId));
                    return Mono.empty();
                })
                .then();
    }

    private Mono<GetTaskResponse> getTask(String taskId) {
        return cloudFoundryClient
                .tasks()
                .get(GetTaskRequest.builder()
                        .taskId(taskId)
                        .build())
                .onErrorResume(NoSuchElementException.class, t -> ExceptionUtils.illegalState("Task with ID %s not found", taskId));
    }

    private static Predicate<GetTaskResponse> isComplete() {
        return task -> TaskState.SUCCEEDED.equals(task.getState()) || TaskState.FAILED.equals(task.getState());
    }

    private static Predicate<GetTaskResponse> isSuccessful() {
        return task -> TaskState.SUCCEEDED.equals(task.getState());
    }
}
