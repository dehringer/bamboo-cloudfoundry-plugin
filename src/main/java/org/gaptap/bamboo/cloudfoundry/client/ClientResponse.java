package org.gaptap.bamboo.cloudfoundry.client;

public class ClientResponse {
    private int status;
    private String response;

    public ClientResponse( int status, String response ){
        this.status = status;
        this.response = response;
    }

    public int getStatus(){
        return this.status;
    }

    public String getResponse(){
        return this.response;
    }
}
