/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.cli;


/**
 * @author David Ehringer
 */
public class ByteSize {

    private static final double BYTE = 1;
    private static final double KILOBYTE = 1024 * BYTE;
    private static final double MEGABYTE = 1024 * KILOBYTE;
    private static final double GIGABTYPE = 1024 * MEGABYTE;
    private static final double TERABYPTE = 1024 * GIGABTYPE;

    public static String format(Long bytes) {


        String unit = null;
        double value = 0;

        if(bytes == null){
            unit = "B";
            value = 0;
        } else if (bytes >= TERABYPTE){
            unit = "T";
            value = bytes/TERABYPTE;
        } else if (bytes >= GIGABTYPE){
            unit = "G";
            value = bytes/GIGABTYPE;
        } else if (bytes >= MEGABYTE){
            unit = "M";
            value = bytes/MEGABYTE;
        } else if (bytes >= KILOBYTE){
            unit = "K";
            value = bytes/KILOBYTE;
        } else if(bytes < KILOBYTE){
            unit = "B";
            value = bytes;
        }

        String stringValue = String.format("%.1f", value);
        stringValue = stringValue.replaceAll(".0", "");
        return stringValue + unit;
    }
}
