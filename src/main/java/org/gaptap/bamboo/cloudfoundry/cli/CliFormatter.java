/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.cli;

import com.atlassian.bamboo.build.logger.BuildLogger;
import org.cloudfoundry.operations.applications.ApplicationDetail;
import org.cloudfoundry.operations.applications.ApplicationSummary;
import org.cloudfoundry.operations.applications.InstanceDetail;
import org.cloudfoundry.operations.services.ServiceInstance;
import org.cloudfoundry.operations.services.ServiceInstanceSummary;
import org.cloudfoundry.operations.services.ServiceInstanceType;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

/**
 * @author David Ehringer
 */
public class CliFormatter {

    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(CliFormatter.class);

    private final BuildLogger buildLogger;
    private final DateFormat dateFormat;
    private final DateFormat sinceDateFormat;
    private final DecimalFormat cpuFormat = new DecimalFormat("0.0");

    public CliFormatter(BuildLogger buildLogger) {
        this.buildLogger = buildLogger;

        dateFormat = new SimpleDateFormat("E MMM dd KK:mm:ss zzz yyyy");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        sinceDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
    }

    public void cfApp(ApplicationDetail applicationDetail) {
        buildLogger.addBuildLogEntry("\n");
        buildLogger.addBuildLogEntry("requested state: " + applicationDetail.getRequestedState());
        buildLogger.addBuildLogEntry("instances: " + applicationDetail.getRunningInstances() + "/" + applicationDetail.getInstances());
        buildLogger.addBuildLogEntry("usage: " + ByteSize.format((long) (applicationDetail.getMemoryLimit() * 1024 * 1024))+ " x " + applicationDetail.getInstances() + " instances");
        buildLogger.addBuildLogEntry("urls: " + formatUrls(applicationDetail));
        buildLogger.addBuildLogEntry("last uploaded: " + dateFormat.format(applicationDetail.getLastUploaded()));
        buildLogger.addBuildLogEntry("stack: " + applicationDetail.getStack());
        buildLogger.addBuildLogEntry("buildpacks: " + applicationDetail.getBuildpacks());
        buildLogger.addBuildLogEntry("\n");

        // The extra logging and exception blocks are here because something seems to be "hanging" here in rare scenarios and this is to help debug it.
        // TODO remove after debugging
        LOG.info("Printing details for all application instances.");
        if(applicationDetail.getInstanceDetails().isEmpty()){
            buildLogger.addBuildLogEntry("There are no running instances of this app.");
        } else {
            try {
                formatAppInstances(applicationDetail);
            } catch (Exception e){
                LOG.error("Unable to print out application instance details.", e);
                LOG.error("ApplicationDetail: " + applicationDetail);
                throw e;
            }
        }
    }

    private String formatUrls(ApplicationDetail applicationDetail) {
        return formatStringList(applicationDetail.getUrls());
    }

    private String formatStringList(List<String> urls) {
        Iterator<String> urlsIter = urls.iterator();
        StringBuilder urlsBuilder = new StringBuilder();
        while(urlsIter.hasNext()){
            urlsBuilder.append(urlsIter.next());
            if(urlsIter.hasNext()){
                urlsBuilder.append(", ");
            }
        }
        return urlsBuilder.toString();
    }

    private void formatAppInstances(ApplicationDetail applicationDetail) {
        TableBuilder tableBuilder = new TableBuilder();
        tableBuilder.addRow("", "state", "since", "cpu", "memory", "disk", "details");
        List<InstanceDetail> details = applicationDetail.getInstanceDetails();
        // TODO remove after debugging
        LOG.info("Building table body");
        for(int i = 0; i < details.size(); i++){
            InstanceDetail detail = details.get(i);
            tableBuilder.addRow("#" + i,
                    detail.getState(),
                    sinceDateFormat.format(detail.getSince()),
                    cpuFormat.format(detail.getCpu()) + "%",
                    ByteSize.format(detail.getMemoryUsage()) + " of " + ByteSize.format(detail.getMemoryQuota()),
                    ByteSize.format(detail.getDiskUsage()) + " of " + ByteSize.format(detail.getDiskQuota()));
        }
        // TODO remove after debugging
        LOG.info("Adding table body to build logs");
        tableBuilder.getTable()
                .stream()
                .forEach(row -> buildLogger.addBuildLogEntry(row));
        // TODO remove after debugging
        LOG.info("Done adding table body to build logs");
    }

    public void cfApps(List<ApplicationSummary> applicationSummaries) {
        TableBuilder tableBuilder = new TableBuilder();
        tableBuilder.addRow("name", "requested state", "instances", "memory", "disk", "urls");
        applicationSummaries.stream()
                .forEach(app -> {
                    tableBuilder.addRow(
                            app.getName(),
                            app.getRequestedState(),
                            app.getRunningInstances() + "/" + app.getInstances(),
                            ByteSize.format((long) (app.getMemoryLimit() * 1024 * 1024)),
                            ByteSize.format((long) (app.getDiskQuota() * 1024 * 1024)),
                            formatStringList(app.getUrls()));
                });

        buildLogger.addBuildLogEntry("\n");
        tableBuilder.getTable()
                .stream()
                .forEach(row -> buildLogger.addBuildLogEntry(row));
    }

    public void cfServices(List<ServiceInstanceSummary> serviceInstances) {
        TableBuilder tableBuilder = new TableBuilder();
        tableBuilder.addRow("name", "service", "plan", "bound apps", "last operation");
        serviceInstances.stream()
                .forEach(serviceInstance -> {
                    String lastOperation = null;
                    if(ServiceInstanceType.MANAGED.equals(serviceInstance.getType())){
                        lastOperation = serviceInstance.getLastOperation();
                    }
                    tableBuilder.addRow(
                            serviceInstance.getName(),
                            serviceInstance.getService(),
                            serviceInstance.getPlan(),
                            formatStringList(serviceInstance.getApplications()),
                            lastOperation
                    );
                });

        buildLogger.addBuildLogEntry("\n");
        tableBuilder.getTable()
                .stream()
                .forEach(row -> buildLogger.addBuildLogEntry(row));
    }

    public void cfService(ServiceInstance serviceInstance) {
        buildLogger.addBuildLogEntry("\n");
        buildLogger.addBuildLogEntry("Service instance: " + serviceInstance.getName());
        buildLogger.addBuildLogEntry("Service: " + serviceInstance.getService());
        if(ServiceInstanceType.MANAGED.equals(serviceInstance.getType())){
            buildLogger.addBuildLogEntry("Plan: " + serviceInstance.getPlan());
            buildLogger.addBuildLogEntry("Description: " + serviceInstance.getDescription());
            buildLogger.addBuildLogEntry("Documentation url: " + serviceInstance.getDocumentationUrl());
            buildLogger.addBuildLogEntry("Dashboard: " + serviceInstance.getDashboardUrl());
            buildLogger.addBuildLogEntry("");
            buildLogger.addBuildLogEntry("Last Operation");
            buildLogger.addBuildLogEntry("Status: " + serviceInstance.getLastOperation() + " " + serviceInstance.getStatus());
            buildLogger.addBuildLogEntry("Message: " + serviceInstance.getMessage());
            buildLogger.addBuildLogEntry("Started: " + serviceInstance.getStartedAt());
            buildLogger.addBuildLogEntry("Updated: " + serviceInstance.getUpdatedAt());
        }
    }
}
