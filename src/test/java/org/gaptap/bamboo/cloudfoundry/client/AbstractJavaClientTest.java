/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;


import org.cloudfoundry.client.CloudFoundryClient;
import org.cloudfoundry.client.v2.applications.ApplicationsV2;
import org.cloudfoundry.client.v2.serviceinstances.ServiceInstances;
import org.cloudfoundry.client.v2.shareddomains.SharedDomains;
import org.cloudfoundry.client.v3.tasks.Tasks;
import org.cloudfoundry.doppler.DopplerClient;
import org.cloudfoundry.operations.CloudFoundryOperations;
import org.cloudfoundry.operations.applications.Applications;
import org.cloudfoundry.operations.domains.Domains;
import org.cloudfoundry.operations.organizations.Organizations;
import org.cloudfoundry.operations.routes.Routes;
import org.cloudfoundry.operations.services.Services;
import org.junit.Before;

import static org.mockito.Mockito.RETURNS_SMART_NULLS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public abstract class AbstractJavaClientTest {

    protected final CloudFoundryClient cloudFoundryClient = mock(CloudFoundryClient.class, RETURNS_SMART_NULLS);
    protected final CloudFoundryOperations cloudFoundryOperations = mock(CloudFoundryOperations.class, RETURNS_SMART_NULLS);
    protected final DopplerClient dopplerClient = mock(DopplerClient.class, RETURNS_SMART_NULLS);
    protected final HealthChecker healthChecker = mock(HealthChecker.class, RETURNS_SMART_NULLS);

    protected final Applications applications = mock(Applications.class, RETURNS_SMART_NULLS);
    protected final ApplicationsV2 applicationsV2 = mock(ApplicationsV2.class, RETURNS_SMART_NULLS);
    protected final Domains domains = mock(Domains.class, RETURNS_SMART_NULLS);
    protected final Organizations organizations = mock(Organizations.class, RETURNS_SMART_NULLS);
    protected final Routes routes = mock(Routes.class, RETURNS_SMART_NULLS);
    protected final Services services = mock(Services.class, RETURNS_SMART_NULLS);

    protected final org.cloudfoundry.client.v2.domains.Domains domainsClient = mock(org.cloudfoundry.client.v2.domains.Domains.class, RETURNS_SMART_NULLS);
    protected final org.cloudfoundry.client.v2.organizations.Organizations organizationsClient = mock(org.cloudfoundry.client.v2.organizations.Organizations.class, RETURNS_SMART_NULLS);
    protected final org.cloudfoundry.client.v2.routes.Routes routesClient = mock(org.cloudfoundry.client.v2.routes.Routes.class, RETURNS_SMART_NULLS);
    protected final ServiceInstances serviceInstancesClient = mock(ServiceInstances.class, RETURNS_SMART_NULLS);
    protected final SharedDomains sharedDomains = mock(SharedDomains.class, RETURNS_SMART_NULLS);
    protected final Tasks tasks = mock(Tasks.class, RETURNS_SMART_NULLS);

    @Before
    public final void mockClient() {
        when(this.cloudFoundryOperations.applications()).thenReturn(this.applications);
        when(this.cloudFoundryOperations.domains()).thenReturn(this.domains);
        when(this.cloudFoundryOperations.organizations()).thenReturn(this.organizations);
        when(this.cloudFoundryOperations.routes()).thenReturn(this.routes);
        when(this.cloudFoundryOperations.services()).thenReturn(this.services);

        when(this.cloudFoundryClient.applicationsV2()).thenReturn(this.applicationsV2);
        when(this.cloudFoundryClient.domains()).thenReturn(this.domainsClient);
        when(this.cloudFoundryClient.organizations()).thenReturn(this.organizationsClient);
        when(this.cloudFoundryClient.routes()).thenReturn(this.routesClient);
        when(this.cloudFoundryClient.serviceInstances()).thenReturn(this.serviceInstancesClient);
        when(this.cloudFoundryClient.sharedDomains()).thenReturn(this.sharedDomains);
        when(this.cloudFoundryClient.tasks()).thenReturn(this.tasks);
    }
}
