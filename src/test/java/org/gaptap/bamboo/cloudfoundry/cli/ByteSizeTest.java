/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.cli;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class ByteSizeTest {

    @Test
    public void nullFormat(){
        assertThat(ByteSize.format(null), is("0B"));
    }

    @Test
    public void bFormat(){
        assertThat(ByteSize.format((long) 512), is("512B"));
    }

    @Test
    public void kbFormat(){
        assertThat(ByteSize.format((long) 1024), is("1K"));
        assertThat(ByteSize.format((long) (1024 + 512)), is("1.5K"));
    }

    @Test
    public void mbFormat(){
        assertThat(ByteSize.format(1024L * 1024 * 213), is("213M"));
        assertThat(ByteSize.format(1024L * 1024 * 213 + 524288), is("213.5M"));
    }

    @Test
    public void gbFormat(){
        assertThat(ByteSize.format(1024L * 1024 * 1024 * 6), is("6G"));
    }

    @Test
    public void tbFormat(){
        assertThat(ByteSize.format(1024L * 1024 * 1024 * 1024 * 12), is("12T"));
    }
}
