/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks;

import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskState;
import org.cloudfoundry.operations.applications.ApplicationDetail;
import org.gaptap.bamboo.cloudfoundry.cli.StubbedBuildLogger;
import org.gaptap.bamboo.cloudfoundry.client.ApplicationConfiguration;
import org.gaptap.bamboo.cloudfoundry.client.BlueGreenConfiguration;
import org.gaptap.bamboo.cloudfoundry.client.PushConfiguration;
import org.junit.Before;
import org.junit.Test;
import reactor.core.publisher.Mono;

import java.util.Date;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_CONFIG_OPTION_YAML;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_LOCATION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_LOCATION_OPTION_DIRECTORY;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.BLUE_GREEN_CUSTOM_DARK_CONFIG;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.BLUE_GREEN_ENABLED;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.BLUE_HEALTH_CHECK_ENDPOINT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.BLUE_HEALTH_CHECK_SKIP_SSL_VALIDATION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.DARK_APP_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.DARK_APP_ROUTE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.DIRECTORY;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.SECONDS_DO_NOT_MONITOR;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.SELECTED_APP_CONFIG_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.START;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.STARTUP_TIMEOUT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.YAML_FILE;
import static org.gaptap.bamboo.cloudfoundry.tasks.dataprovider.TargetTaskDataProvider.DISABLE_FOR_BUILD_PLANS;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author David Ehringer
 */
public class PushTaskTest extends AbstractTaskTest {

    private PushTask task;

    @Before
    public void init() {
        createDefaultConfigMap();

        task = new PushTask(encryptionService);
        task.setCloudFoundryServiceFactory(cloudFoundryServiceFactory);
    }

    private void createDefaultConfigMap() {
        configurationMap = new ConfigurationMapImpl();
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);
        configurationMap.put(START, "false");
        configurationMap.put(STARTUP_TIMEOUT, SECONDS_DO_NOT_MONITOR);
        configurationMap.put(SELECTED_APP_CONFIG_OPTION, APP_CONFIG_OPTION_YAML);
        configurationMap.put(YAML_FILE, "src/test/resources/manifest-min.yml");
        configurationMap.put(APP_LOCATION, APP_LOCATION_OPTION_DIRECTORY);
        configurationMap.put(DIRECTORY, ".");
        configurationMap.put(BLUE_GREEN_ENABLED, "false");
    }

    private ApplicationDetail applicationDetail(){
        return ApplicationDetail.builder()
                .buildpack("test-application-summary-buildpack")
                .id("test-application-summary-id")
                .diskQuota(1024)
                .instances(1)
                .memoryLimit(1024)
                .runningInstances(1)
                .lastUploaded(new Date(0))
                .name("test-application-summary-name")
                .requestedState("test-application-summary-state")
                .stack("test-stack-entity-name")
                .url("test-route-host.test-domain-name")
                .build();
    }

    @Test
    public void whenDisableForBuildPlansIsSelectedAndTheTaskExecutesInABuildPlanTheTaskFails()
            throws TaskException {
        // Given
        runtimeTaskContext.put(DISABLE_FOR_BUILD_PLANS, "true");

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.ERROR));
    }

    @Test
    public void blueGreenParametersCanBeNullSoWeCanSupportUpgradesFromVersionsPriorTo3_0() throws TaskException {
        // Given
        when(cloudFoundryService.push(any(ApplicationConfiguration.class), any(PushConfiguration.class)))
                .thenReturn(Mono.just(applicationDetail()));
        configurationMap.remove(BLUE_GREEN_ENABLED);

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.SUCCESS));
        verify(cloudFoundryService).push(any(ApplicationConfiguration.class), any(PushConfiguration.class));
    }

    @Test
    public void whenBlueGreenIsEnabledABlueGreenDeploymentIsExecuted() throws TaskException {
        // Given
        when(cloudFoundryService.push(any(ApplicationConfiguration.class), any(PushConfiguration.class), any(BlueGreenConfiguration.class)))
                .thenReturn(Mono.just(applicationDetail()));
        configurationMap.put(BLUE_GREEN_ENABLED, "true");

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.SUCCESS));
        verify(cloudFoundryService).push(any(ApplicationConfiguration.class), any(PushConfiguration.class), any(BlueGreenConfiguration.class));
    }

    @Test
    public void blueGreenParametersAreSet() throws TaskException {
        // Given
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .useCustomDarkAppConfiguration(true)
                .customDarkAppName("green-app")
                .customDarkRoute("green.cf.com")
                .healthCheckEndpoint("/health")
                .skipSslValidation(true)
                .build();

        when(cloudFoundryService.push(any(ApplicationConfiguration.class), any(PushConfiguration.class), any(BlueGreenConfiguration.class)))
                .thenReturn(Mono.just(applicationDetail()));
        configurationMap.put(BLUE_GREEN_ENABLED, "true");
        configurationMap.put(BLUE_GREEN_CUSTOM_DARK_CONFIG, "true");
        configurationMap.put(DARK_APP_NAME, blueGreenConfiguration.customDarkAppName());
        configurationMap.put(DARK_APP_ROUTE, blueGreenConfiguration.customDarkRoute());
        configurationMap.put(BLUE_HEALTH_CHECK_ENDPOINT, blueGreenConfiguration.healthCheckEndpoint());
        configurationMap.put(BLUE_HEALTH_CHECK_SKIP_SSL_VALIDATION, "true");

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.SUCCESS));
        verify(cloudFoundryService).push(any(ApplicationConfiguration.class), any(PushConfiguration.class), eq(blueGreenConfiguration));
    }

    @Test
    public void blueGreenParametersAreSetWhenCustomConfigSetToFalseButCustomAppConfigIsInConfigMap() throws TaskException {
        // This is the scenario where someone selected and saved:
        // BLUE_GREEN_CUSTOM_DARK_CONFIG = true
        // DARK_APP_NAME = something
        // DARK_APP_ROUTE = something
        //
        // And then they subsequently unchecked BLUE_GREEN_CUSTOM_DARK_CONFIG. DARK_APP_NAME and DARK_APP_ROUTE would still be configured
        // in the configurationMap but we want to ignore them.

        // Given
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .useCustomDarkAppConfiguration(false)
                .healthCheckEndpoint("/health")
                .skipSslValidation(true)
                .build();

        when(cloudFoundryService.push(any(ApplicationConfiguration.class), any(PushConfiguration.class), any(BlueGreenConfiguration.class)))
                .thenReturn(Mono.just(applicationDetail()));
        configurationMap.put(BLUE_GREEN_ENABLED, "true");
        configurationMap.put(BLUE_GREEN_CUSTOM_DARK_CONFIG, "false");
        configurationMap.put(DARK_APP_NAME, "an-outdated-name");
        configurationMap.put(DARK_APP_ROUTE, "outdated.route.com");
        configurationMap.put(BLUE_HEALTH_CHECK_ENDPOINT, blueGreenConfiguration.healthCheckEndpoint());
        configurationMap.put(BLUE_HEALTH_CHECK_SKIP_SSL_VALIDATION, "true");

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.SUCCESS));
        verify(cloudFoundryService).push(any(ApplicationConfiguration.class), any(PushConfiguration.class), eq(blueGreenConfiguration));
    }

    // TODO commented this test out because the clearBuildLog() was removed in Bamboo 7 and this needs an alternative.
//    @Test
//    public void whenAnExceptionOccursTheMessageIsWritenToTheBuildLogs() throws TaskException {
//        // Given
//        StubbedBuildLogger logger = new StubbedBuildLogger();
//        when(taskContext.getBuildLogger()).thenReturn(logger);
//
//        when(cloudFoundryService.push(any(ApplicationConfiguration.class), any(PushConfiguration.class)))
//                .thenReturn(Mono.error(new IllegalArgumentException("some problem start")));
//        when(cloudFoundryService.push(any(ApplicationConfiguration.class), any(PushConfiguration.class), any(BlueGreenConfiguration.class)))
//                .thenReturn(Mono.error(new IllegalArgumentException("some problem blue/green")));
//
//        // When
//        TaskResult result = task.execute(taskContext);
//        // Then
//        assertThat(result.getTaskState(), is(TaskState.ERROR));
//        assertThat(logger.getLastNLogEntries(1).get(0).getUnstyledLog(), is("Unable to push app: some problem start"));
//
//        // When
//        logger.clearBuildLog();
//        configurationMap.put(START, "true");
//        result = task.execute(taskContext);
//        // Then
//        assertThat(result.getTaskState(), is(TaskState.ERROR));
//        assertThat(logger.getLastNLogEntries(1).get(0).getUnstyledLog(), is("Unable to push app: some problem start"));
//
//        // When
//        logger.clearBuildLog();
//        configurationMap.put(START, "true");
//        configurationMap.put(BLUE_GREEN_ENABLED, "true");
//        result = task.execute(taskContext);
//        // Then
//        assertThat(result.getTaskState(), is(TaskState.ERROR));
//        assertThat(logger.getLastNLogEntries(1).get(0).getUnstyledLog(), is("Unable to push app with a blue/green deployment: some problem blue/green"));
//    }

    @Test
    public void startupTimeoutIsOptional() throws TaskException {
        // Given
        when(cloudFoundryService.push(any(ApplicationConfiguration.class), any(PushConfiguration.class)))
                .thenReturn(Mono.just(applicationDetail()));
        configurationMap.put(STARTUP_TIMEOUT, null);

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.SUCCESS));
        verify(cloudFoundryService).push(any(ApplicationConfiguration.class), any(PushConfiguration.class));
    }

    @Test
    public void whenTheGivenFilePatternDoesNotMatchOtherSimilarNamesAreWrittenToTheBuildLogs() throws TaskException {
        // Given
        configurationMap.put(YAML_FILE, "src/test/resources/manifest-idontexist.yml");
        StubbedBuildLogger logger = new StubbedBuildLogger();
        when(taskContext.getBuildLogger()).thenReturn(logger);

        // When
        TaskResult result = task.execute(taskContext);
        // Then
        assertThat(result.getTaskState(), is(TaskState.ERROR));
        assertThat(logger.getLastNLogEntries(1).get(0).getUnstyledLog(), containsString("Manifest file not found"));
        assertThat(logger.getLastNLogEntries(1).get(0).getUnstyledLog(), containsString("These files were found in the same directory"));
    }
}
