/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.utils;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APPLICATION_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.BUILDPACK_URL;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.COMMAND;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.DISK_QUOTA;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.ENVIRONMENT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.INSTANCES;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.MEMORY;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.NO_HOSTNAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.SERVICES;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.HEALTH_CHECK_TIMEOUT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.ROUTES;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import org.gaptap.bamboo.cloudfoundry.client.ApplicationConfiguration;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.task.TaskContext;

/**
 * @author David Ehringer
 * 
 */
public class ApplicationConfigurationMapperTest {

	private ApplicationConfigurationMapper mapper = new ApplicationConfigurationMapper();

	private ConfigurationMap configMap;
	private TaskContext taskContext;

	@Before
	public void setup() {
		configMap = new ConfigurationMapImpl();
		configMap.put(APPLICATION_NAME, "test-app");
		configMap.put(COMMAND, "");
        configMap.put(BUILDPACK_URL, "");
		configMap.put(ENVIRONMENT, "");
		configMap.put(INSTANCES, "1");
		configMap.put(MEMORY, "256");
		configMap.put(DISK_QUOTA, "1024");
        configMap.put(HEALTH_CHECK_TIMEOUT, "80");
		configMap.put(SERVICES, "");
		configMap.put(ROUTES, "bamboo.cloudfoundry.com");

		taskContext = mock(TaskContext.class);
		when(taskContext.getConfigurationMap()).thenReturn(configMap);
	}

	@Test
	public void anEmptyUrlIsExtractedToAnEmptyList() {
		configMap.put(ROUTES, "");

		ApplicationConfiguration appConfig = mapper.from(taskContext);

		assertThat(appConfig.routes().size(), is(0));
	}

	@Test
	public void anEmptyUrlWithSpacesIsExtractedToAnEmptyList() {
		configMap.put(ROUTES, " ");

		ApplicationConfiguration appConfig = mapper.from(taskContext);

		assertThat(appConfig.routes().size(), is(0));
	}

	@Test
	public void aSingleUrlIsExtractedToASingleUrl() {
		String url = "bamboo.cloudfoundry.com";
		configMap.put(ROUTES, url);

		ApplicationConfiguration appConfig = mapper.from(taskContext);

		assertThat(appConfig.routes().size(), is(1));
		assertThat(appConfig.routes().get(0), is(url));
	}

	@Test
	public void aSingleUrlWithLeadingAndTrailingSpacesIsExtractedToASingleUrlWithNoWhitespace() {
		String url = " bamboo.cloudfoundry.com   ";
		configMap.put(ROUTES, url);

		ApplicationConfiguration appConfig = mapper.from(taskContext);

		assertThat(appConfig.routes().size(), is(1));
		assertThat(appConfig.routes().get(0), is("bamboo.cloudfoundry.com"));
	}

	@Test
	public void twoUrlsAreExtractedToTwoUrls() {
		String url = "bamboo.cloudfoundry.com,dave.cloudfoundry.com";
		configMap.put(ROUTES, url);

		ApplicationConfiguration appConfig = mapper.from(taskContext);

		assertThat(appConfig.routes().size(), is(2));
		assertThat(appConfig.routes().get(0), is("bamboo.cloudfoundry.com"));
		assertThat(appConfig.routes().get(1), is("dave.cloudfoundry.com"));
	}

	@Test
	public void threeUrlsAreExtractedToThreeUrls() {
		String url = "bamboo.cloudfoundry.com,dave.cloudfoundry.com,test.cloudfoundry.com";
		configMap.put(ROUTES, url);

		ApplicationConfiguration appConfig = mapper.from(taskContext);

		assertThat(appConfig.routes().size(), is(3));
		assertThat(appConfig.routes().get(0), is("bamboo.cloudfoundry.com"));
		assertThat(appConfig.routes().get(1), is("dave.cloudfoundry.com"));
		assertThat(appConfig.routes().get(2), is("test.cloudfoundry.com"));
	}

	@Test
	public void multipleUrlsWithSpacesAreExtractedWithoutSpaces() {
		String url = "bamboo.cloudfoundry.com   , dave.cloudfoundry.com, test.cloudfoundry.com  ";
		configMap.put(ROUTES, url);

		ApplicationConfiguration appConfig = mapper.from(taskContext);

		assertThat(appConfig.routes().size(), is(3));
		assertThat(appConfig.routes().get(0), is("bamboo.cloudfoundry.com"));
		assertThat(appConfig.routes().get(1), is("dave.cloudfoundry.com"));
		assertThat(appConfig.routes().get(2), is("test.cloudfoundry.com"));
	}

	@Test
	public void multipleServicesWithSpacesAreExtractedWithoutSpaces() {
		String services = " mysql   , redis, rabbitmq  ";
		configMap.put(SERVICES, services);

		ApplicationConfiguration appConfig = mapper.from(taskContext);

		assertThat(appConfig.serviceBindings().size(), is(3));
		assertThat(appConfig.serviceBindings().get(0), is("mysql"));
		assertThat(appConfig.serviceBindings().get(1), is("redis"));
		assertThat(appConfig.serviceBindings().get(2), is("rabbitmq"));
	}

	@Test
	public void noHostname(){
		String url = "cloudfoundry.com,dave.cloudfoundry.com";
		configMap.put(ROUTES, url);
		configMap.put(NO_HOSTNAME, "true");

		ApplicationConfiguration appConfig = mapper.from(taskContext);

		assertTrue(appConfig.noHostname());
		assertThat(appConfig.domains().size(), is(2));
		assertThat(appConfig.domains().get(0), is("cloudfoundry.com"));
		assertThat(appConfig.domains().get(1), is("dave.cloudfoundry.com"));
	}

	@Test
	public void anEmptyEnvironmentStringResultsInAnEmptyEnvironmentList() {
		configMap.put(ENVIRONMENT, "");
		ApplicationConfiguration appConfig = mapper.from(taskContext);
		assertThat(appConfig.environment().size(), is(0));
	}

	@Test
	public void anEmptyEnvironmentStringWithSpacesResultsInAnEmptyEnvironmentList() {
		configMap.put(ENVIRONMENT, "   ");
		ApplicationConfiguration appConfig = mapper.from(taskContext);
		assertThat(appConfig.environment().size(), is(0));
	}

	@Test
	public void anEmptyEnvironmentStringWithNewlinesResultsInAnEmptyEnvironmentList() {
		configMap.put(ENVIRONMENT, "\n\n");
		ApplicationConfiguration appConfig = mapper.from(taskContext);
		assertThat(appConfig.environment().size(), is(0));
	}

	@Test
	public void anEmptyEnvironmentStringWithNewlinesAndSpacesResultsInAnEmptyEnvironmentList() {
		configMap.put(ENVIRONMENT, "\n \n    ");
		ApplicationConfiguration appConfig = mapper.from(taskContext);
		assertThat(appConfig.environment().size(), is(0));
	}

	@Test
	public void anEnvironmentStringWithOnePairOfPropertiesResultInAnEnvironmentListWithOneProperty() {
		configMap.put(ENVIRONMENT, "my.variable=test");
		ApplicationConfiguration appConfig = mapper.from(taskContext);
		assertThat(appConfig.environment().size(), is(1));
		assertThat(appConfig.environment().get("my.variable"), is("test"));
	}

	@Test
	public void anEnvironmentStringWithOnePairOfPropertiesWithSpacesResultInAnEnvironmentListWithOneProperty() {
		configMap.put(ENVIRONMENT, " my.variable = test  ");
		ApplicationConfiguration appConfig = mapper.from(taskContext);
		assertThat(appConfig.environment().size(), is(1));
		assertThat(appConfig.environment().get("my.variable"), is("test"));
	}

	@Test
	public void anEnvironmentStringWithMultiplePairsOfPropertiesWithSpacesResultInAnEnvironmentListWithMultiplePropertiesWithoutSpaces() {
		String env = " my.variable = test  \n" //
				+ "two=second\n" //
				+ "three.5=\n" //
				+ "three= the third \n" //
				+ " the fourth = four";
		configMap.put(ENVIRONMENT, env);
		ApplicationConfiguration appConfig = mapper.from(taskContext);
		assertThat(appConfig.environment().size(), is(5));
		assertThat(appConfig.environment().get("my.variable"), is("test"));
		assertThat(appConfig.environment().get("two"), is("second"));
		assertThat(appConfig.environment().get("three"), is("the third"));
		assertThat(appConfig.environment().get("three.5"), is(""));
		assertThat(appConfig.environment().get("the fourth"), is("four"));
	}

	@Test
	public void anEnvironmentStringWithJavaOptsResultIsParsed() {
		configMap.put(ENVIRONMENT, " JAVA_OPTS = \"-Done=a -Dtwo=b -Dthree.basedir=/app/logs\"  ");
		ApplicationConfiguration appConfig = mapper.from(taskContext);
		assertThat(appConfig.environment().size(), is(1));
		assertThat(appConfig.environment().get("JAVA_OPTS"), is("\"-Done=a -Dtwo=b -Dthree.basedir=/app/logs\""));
	}
	
	@Test
	public void anEmptyBuildpackUrlIsConvertedToAnEmptyArray(){
	    ApplicationConfiguration appConfig = mapper.from(taskContext);
        assertThat(appConfig.buildpacks(), is(Collections.emptyList()));
	}
    
    @Test
    public void anEmptyCommandIsConvertedToAnEmptyString(){
        ApplicationConfiguration appConfig = mapper.from(taskContext);
        assertThat(appConfig.command(), is(""));
    }
    
    @Test
    public void diskQuotaIsMappedWhenProvided(){
        ApplicationConfiguration appConfig = mapper.from(taskContext);
        assertThat(appConfig.diskQuota(), is(1024));
    }
    @Test
    public void diskQuotaIsSetToTheDefaultWhenNotProvided(){
        configMap.put(DISK_QUOTA, "");
        ApplicationConfiguration appConfig = mapper.from(taskContext);
        assertThat(appConfig.diskQuota(), is(ApplicationConfiguration.DEFAULT_DISK_QUOTA));
    }
    
    @Test
    public void timeoutIsMappedWhenProvided(){
        ApplicationConfiguration appConfig = mapper.from(taskContext);
        assertThat(appConfig.healthCheckTimeout(), is(80));
    }
    @Test
    public void timeoutIsSetToNullWhenNotProvided(){
        configMap.put(HEALTH_CHECK_TIMEOUT, "");
        ApplicationConfiguration appConfig = mapper.from(taskContext);
        assertThat(appConfig.healthCheckTimeout(), is(nullValue()));
    }
    
}
