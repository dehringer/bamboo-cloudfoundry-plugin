/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks;

import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskState;
import org.gaptap.bamboo.cloudfoundry.cli.StubbedBuildLogger;
import org.junit.Before;
import org.junit.Test;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_CONFIG_OPTION_YAML;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.SELECTED_APP_CONFIG_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServicePushTaskConfigurator.FILE;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @author David Ehringer
 */
public class ServicePushTaskTest extends AbstractTaskTest {

    private ServicePushTask task;

    @Before
    public void init() {
        createDefaultConfigMap();

        task = new ServicePushTask(encryptionService);
        task.setCloudFoundryServiceFactory(cloudFoundryServiceFactory);
    }

    private void createDefaultConfigMap() {
        configurationMap = new ConfigurationMapImpl();
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);

        configurationMap.put(SELECTED_APP_CONFIG_OPTION, APP_CONFIG_OPTION_YAML);
        configurationMap.put(FILE, "src/test/resources/manifest-min.yml");
    }


    @Test
    public void whenTheGivenFilePatternDoesNotMatchOtherSimilarNamesAreWrittenToTheBuildLogs() throws TaskException {
        // Given
        configurationMap.put(FILE, "src/test/resources/manifest-idontexist.yml");
        StubbedBuildLogger logger = new StubbedBuildLogger();
        when(taskContext.getBuildLogger()).thenReturn(logger);

        // When
        TaskResult result = task.execute(taskContext);
        // Then
        assertThat(result.getTaskState(), is(TaskState.ERROR));
        assertThat(logger.getLastNLogEntries(1).get(0).getUnstyledLog(), containsString("Found 0 files matching the pattern"));
        assertThat(logger.getLastNLogEntries(1).get(0).getUnstyledLog(), containsString("These files were found in the same directory"));
    }
}
